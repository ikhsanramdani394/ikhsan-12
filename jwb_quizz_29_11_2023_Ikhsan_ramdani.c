#include <stdio.h>

int main() {
    int n, i;
    float total = 0;

    printf("Masukan banyaknya data: ");
    scanf("%d", &n);

    if (n <= 0) {
        printf("Banyaknya data harus lebih dari 0.\n");
        return 1;
    }

    for (i = 1; i <= n; i++) {
        int data;
        printf("Masukan Data ke-%d: ", i);
        scanf("%d", &data);
        total += data;
    }

    printf("\nBanyaknya Data       : %d\n", n);
    printf("Total Nilai Data     : %.2f\n", total);
    printf("Rata-rata nilai data : %.2f\n", total / n);

    return 0;
}


/*
Masukan Data Ke-1 : 1
Masukan Data Ke-2 : 2
Masukan Data Ke-3 : 3
Masukan Data Ke-4 : 0

Masukan banyaknya data: 3
Masukan Data ke-1: 1
Masukan Data ke-2: 2
Masukan Data ke-3: 3
Banyaknya Data       : 3
Total Nilai Data     : 6.00
Rata-rata nilai data : 2.00
*\

